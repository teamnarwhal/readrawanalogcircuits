# Netduino Analog Read Example

----

Unpublished work © 2014, The Narwhal Group

All Rights Reserved

THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF THE NARWHAL GROUP.

The copyright notice above does not evidence any
actual or intended publication of such source code.

----

## Overview

This project contains .NET Micro Framework source code for an application that reads analog circuits 0-3 and dumps the reads to the console. This is a simple test program for debugging shields. The application hosted on a [Netduino Plus 2](http://www.netduino.com/) board.

## Configuration

The application is written using .NET Micro Framework v4.3.1 so the Netduino Plus 2 must be flashed with the correct firmware. See [here](https://wiki.narwhalgroup.com/twiki/bin/view/Main/NetduinoPlus2) for more information on setting up the board.

## Building

To checkout, clone this repository and once completed, open the `ReadRawAnalogCicrcuits.sln` file with Visual Studio 2012 and build the solution.
