﻿using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware.Netduino;

namespace ReadRawAnalogInputs
{
    public class Program
    {

        public static void Main()
        {
            // turn circuit on
            var d0 = new OutputPort(Pins.GPIO_PIN_D9, true);
            d0.Write(true);

            // create analog inputs
            AnalogInput[] inputs = { new AnalogInput(Microsoft.SPOT.Hardware.Cpu.AnalogChannel.ANALOG_0),
                new AnalogInput(Microsoft.SPOT.Hardware.Cpu.AnalogChannel.ANALOG_1),
                new AnalogInput(Microsoft.SPOT.Hardware.Cpu.AnalogChannel.ANALOG_2),
                new AnalogInput(Microsoft.SPOT.Hardware.Cpu.AnalogChannel.ANALOG_3)
            };

            // read forever
            var inputSums = new double[inputs.Length];
            var numSamples = 0;
            while (true)
            {
                var inputReads = new int[inputs.Length];
                for (var n = 0; n < inputs.Length; ++n)
                {
                    inputReads[n] = inputs[n].ReadRaw();
                    inputSums[n] += inputReads[n];
                    Thread.Sleep(100);
                }

                ++numSamples;
                var s = numSamples + ": ";
                for (var n = 0; n < inputs.Length; ++n)
                    s += "A" + n + " = " + inputReads[n] + "/" + (inputSums[n] / numSamples).ToString("f1") + (n < (inputs.Length - 1) ? ", " : string.Empty);

                Debug.Print(s);

                if (numSamples == 50)
                {
                    Debug.Print("--- resetting ---");
                    for (var n = 0; n < inputs.Length; ++n)
                        inputSums[n] = 0;
                    numSamples = 0;
                }
            }
        }
    }
}
